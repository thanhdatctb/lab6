﻿using Lab6._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab6._0
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<User> items = new List<User>();
            items.Add(new User()
            {
                Name = "Nguyen Van A",
                Age = 42,
                Mail =
           "A@gmail.com"
            });
            items.Add(new User()
            {
                Name = "Nguyen Van A",
                Age = 39,
                Mail =
           "B@gmail.com"
            });
            items.Add(new User()
            {
                Name = "Nguyen Van A",
                Age = 7,
                Mail =
           "C@gmail.com"
            });
            lvUsers.ItemsSource = items;
        }
    }
}
