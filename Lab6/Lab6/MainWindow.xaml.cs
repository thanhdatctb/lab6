﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _operator;
        private int first;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonClearAll_Click(object sender, RoutedEventArgs e)
        {
            this.txtNumberDisplay.Text = "";
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            string text = this.txtNumberDisplay.Text;
            this.txtNumberDisplay.Text = text.Remove(text.Length - 1, 1);
        }

        private void ButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.first = int.Parse(this.txtNumberDisplay.Text);
                this.txtNumberDisplay.Text = "";
                this._operator = "+";
            }catch(Exception ex)
            {

            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonNumber_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            SetNumberDisplay(btn.Content.ToString());
        }
        private void SetNumberDisplay(string textNumber)
        {
            if (this.txtNumberDisplay.Text == "0" || this.txtNumberDisplay.Text =="+")
            {
                this.txtNumberDisplay.Text = textNumber;
            }
            else
                this.txtNumberDisplay.Text += textNumber;
        }
        private float cal(int first, int last)
        {
            float result = 0;
            switch (this._operator)
            {
                case "+":
                    // code block
                    result = first + last;
                    break;
                case "-":
                    // code block
                    result = first - last;
                    break;
                case "*":
                    result = first * last;
                    break;
                case "/":
                    result = first / last;
                    break;
                default:
                    // code block
                    break;
            }
            return result;
        }
        private void ButtonEquals_Click(object sender, RoutedEventArgs e)
        {
            int last = int.Parse(this.txtNumberDisplay.Text);
            this.txtNumberDisplay.Text = cal(first, last).ToString();
        }

        private void ButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            
            this.first = int.Parse(this.txtNumberDisplay.Text.ToString());
            this.txtNumberDisplay.Text = "";
            this._operator = "-";
        }

        private void ButtonTimes_Click(object sender, RoutedEventArgs e)
        {
            
            this.first = int.Parse(this.txtNumberDisplay.Text);
            this.txtNumberDisplay.Text = "";
            this._operator = "*";
        }

        private void ButtonDivide_Click(object sender, RoutedEventArgs e)
        {
            
            this.first = int.Parse(this.txtNumberDisplay.Text);
            this.txtNumberDisplay.Text = "";
            this._operator = "/";
        }
    }
}
